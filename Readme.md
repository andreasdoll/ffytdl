# ffytdl
## Download youtube videos from firefox bookmarks

``ffytdl`` downloads youtube videos from firefox bookmarks, stores the audio (mp3) and video files using the video title as filename, and deletes the bookmarks afterwards if the download was successful.


## Install
Make the script executable

    $ chmod +x ffytdl

and modify `$outDir` to define the output directory for audio and video files.


## License
MIT, see ``LICENSE``


## Todo
* parametrize directory to store
* handle if there are more than one profile folders
